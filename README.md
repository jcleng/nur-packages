# nur-packages

**My personal [NUR](https://github.com/nix-community/NUR) repository**


<!-- ![Build and populate cache](https://github.com/jcleng/nur-packages/workflows/Build%20and%20populate%20cache/badge.svg) -->
![gitlab action status](https://jihulab.com/jcleng/nur-packages/badges/master/pipeline.svg)

<!--
Uncomment this if you use travis:

[![Build Status](https://travis-ci.com/<YOUR_TRAVIS_USERNAME>/nur-packages.svg?branch=master)](https://travis-ci.com/<YOUR_TRAVIS_USERNAME>/nur-packages)
-->
[![Cachix Cache](https://img.shields.io/badge/cachix-jcleng--dev-blue.svg)](https://jcleng-dev.cachix.org)

- 使用

```shell
# 是基于nixpkgs-unstable进行构建的,所以不管是nix-env还是nix-os都需要添加这个channel
nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
sudo nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs

# nix-env使用:
# add channel
nix-channel --add https://jihulab.com/jcleng/nur-packages/-/archive/master/nur-packages-master.tar.gz jcleng-dev

# use cachix binary file
cachix use jcleng-dev

# update
nix-channel --list
nix-channel --update

# search
nix-env -aqP|grep jcleng-dev.
nix-env -aqP|grep jcleng-dev.kangle

# example install
nix-env -iA jcleng-dev.kangle
# 使用自定义参数是需要增加cachix地址
nix-env -iA jcleng-dev.kangle --option substituters "http://cache.leng2011.icu https://jcleng-dev.cachix.org"

# nixos使用: /etc/nixos/configuration.nix
{
  nixpkgs.config.packageOverrides = pkgs: {
    nur-jcleng-dev = import (builtins.fetchTarball "https://jihulab.com/jcleng/nur-packages/-/archive/master/nur-packages-master.tar.gz") {
      inherit pkgs;
    };
  };
}
# 配置安装
nur-jcleng-dev.kangle
```

- [action build cachix nightly](https://github.com/jcleng/nur-jcleng-dev-action)

- 软件列表

| name | readme |
| - | - |
| kangle | [readme.md](./pkgs/kangle/readme.md) |
| lightlyshaders | [readme.md](./pkgs/lightly-shaders/readme.md) |
