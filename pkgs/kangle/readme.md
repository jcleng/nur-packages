- 使用说明

默认是安装到/nix/store系统目录,是不可写的,需要手动复制配置文件到可写的用户目录

```shell
which kangle
# /home/jcleng/.nix-profile/bin/kangle
ls -lash /home/jcleng/.nix-profile/bin/kangle
    #  4 lrwxrwxrwx    1 root     root          74 Jan  1  1970 /home/jcleng/.nix-profile/bin/kangle -> /nix/store/pz6b501gp3yd9m9lq8x5znccar5jp5cb-kangle-dev-3.5.13.2/bin/kangle
ls /nix/store/pz6b501gp3yd9m9lq8x5znccar5jp5cb-kangle-dev-3.5.13.2/
# bin       etc       include   webadmin  www

mkdir ~/kangle
cp -r /nix/store/pz6b501gp3yd9m9lq8x5znccar5jp5cb-kangle-dev-3.5.13.2/* ~/kangle/

# 运行, 因为80端口限制,需要用root用户来运行
sudo su
/home/jcleng/kangle/bin/kangle -d 3

# 其他: 在nix-on-dorid无法正确响应数据
```
