{ stdenv, fetchgit, lib, pkgs }:

stdenv.mkDerivation rec {
  pname = "kangle-dev";
  version = "3.5.13.2";

  # https://github.com/NixOS/nixpkgs/blob/da5c3bde300e33ace4fdef51abd3b543123a3be2/pkgs/build-support/fetchgit/default.nix#L13
  src = fetchgit {
    url = "https://github.com/jcleng/nix-kangle-3.5.13.2.git";
    branchName = "master";
    # commit id
    rev = "50087ae822c9cdab064550f814a256818238b6df";
    sha256 = "jIl3sph+0a95OAcIHqGU16nqzip8g8Khzq5lu60L2DY=";
  };

  # 构建时依赖
  nativeBuildInputs = [
    pkgs.coreutils
    pkgs.gcc9
    pkgs.pcre
    pkgs.openssl
    pkgs.libaio
    pkgs.zlib
  ];
  # 运行时依赖
  buildInputs = [
    pkgs.pcre
    pkgs.libaio
    pkgs.openssl
    pkgs.zlib
  ];

  configurePhase = ''
    declare -xp
  '';
  buildPhase = ''
    cd src
    ./configure --prefix=$out --exec-prefix=$out
    make -j4
  '';
  installPhase = ''
    make install
  '';

  meta = with lib; {
    description = " high-performance web server.";
    longDescription = ''
    kangle is a light, high-performance web server/reverse proxy.include a http manage console. Full support access control. memory/disk cache. and more kangle web server features
    '';
    homepage = https://github.com/jcleng/nix-kangle-3.5.13.2;
    changelog = "https://github.com/jcleng/nix-kangle-3.5.13.2";
    # license = licenses.none;
    platforms = platforms.all;
  };
}
