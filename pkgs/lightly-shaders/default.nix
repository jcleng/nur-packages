{ lib
, stdenv
, fetchgit
, cmake
, extra-cmake-modules
, libsForQt5
, libepoxy
}:

stdenv.mkDerivation rec {
  pname = "lightlyshaders";
  version = "v2.0.0";

  src = fetchgit {
    url = "https://github.com/a-parhom/LightlyShaders.git";
    # url = "https://ghproxy.com/https://github.com/a-parhom/LightlyShaders.git";
    branchName = "master";
    # commit id
    rev = "ede8fb483d8c3f36698062423ef122f367ac66d7";
    sha256 = "sha256-RlZ3T7vm6mD/dSKZX3ESGwjZyUGstIvspPmGqjULQy8=";
  };

  #cmakeFlags = [ ];

  nativeBuildInputs = [
    cmake
    extra-cmake-modules
  ];

  buildInputs = [
    libsForQt5.kwindowsystem
    libsForQt5.plasma-framework
    libsForQt5.systemsettings
    libsForQt5.kdecoration
    libsForQt5.kinit
    libsForQt5.kwin
    libepoxy
    libsForQt5.kdelibs4support
    libsForQt5.qt5.wrapQtAppsHook
  ];

  postConfigure = ''
    substituteInPlace cmake_install.cmake \
      --replace "${libsForQt5.kdelibs4support}" "$out"
  '';

  meta = with lib; {
    description = "This version has almost zero performance impact, as well as correctly works with stock Plasma effects";
    license = licenses.mit;
    maintainers = with maintainers; [ ];
    homepage = "https://github.com/a-parhom/LightlyShaders";
    inherit (libsForQt5.kwindowsystem.meta) platforms;
  };
}
