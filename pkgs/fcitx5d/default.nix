{ lib
, stdenv
, fetchurl
, fetchgit
, fetchFromGitHub
, fetchpatch
, pkg-config
, cmake
, extra-cmake-modules
, cairo
, pango
, fribidi
, fmt
, wayland
, systemd
, wayland-protocols
, json_c
, isocodes
, xkeyboard_config
, enchant
, gdk-pixbuf
, libGL
, libevent
, libuuid
, libselinux
, libXdmcp
, libsepol
, libxkbcommon
, libthai
, libdatrie
, xcbutilkeysyms
, pcre
, xcbutilwm
, xcb-imdkit
, libxkbfile
, librime
, rime-data
, lua5_3_compat
}:

stdenv.mkDerivation rec {
  pname = "fcitx5d";
  version = "5.1.0";

  src = ./src;

  nativeBuildInputs = [
    cmake
    extra-cmake-modules
    pkg-config
  ];

  configurePhase = ''
    declare -xp
  '';
  buildInputs = [
    fmt
    isocodes
    cairo
    enchant
    pango
    libthai
    libdatrie
    fribidi
    systemd
    gdk-pixbuf
    wayland
    wayland-protocols
    json_c
    libGL
    libevent
    libuuid
    libselinux
    libsepol
    libXdmcp
    libxkbcommon
    pcre
    xcbutilwm
    xcbutilkeysyms
    xcb-imdkit
    xkeyboard_config
    libxkbfile
    librime
    lua5_3_compat
  ];
  buildPhase = ''
    export MAKEFLAGS=-j32

    tar xvf fcitx5.5.1.0.tar.gz
    cp en_dict-20121020.tar.gz ./fcitx5-5.1.0/src/modules/spell/en_dict-20121020.tar.gz
    cd fcitx5-5.1.0
    mkdir build && cd build && cmake -DCMAKE_INSTALL_PREFIX=$out .. && make && make install
    cd ../..

    tar xvf fcitx5-rime.5.1.0.tar.gz
    cd fcitx5-rime-5.1.0
    # FIXNME: DRIME_DATA_DIR
    mkdir build && cd build && cmake -DCMAKE_INSTALL_PREFIX=$out -DRIME_DATA_DIR=$out .. && make && make install
    cd ../..

    tar xvf fcitx5-lua.5.0.10.tar.gz
    cd fcitx5-lua-5.0.10
    mkdir build && cd build && cmake -DCMAKE_INSTALL_PREFIX=$out -DLUA_INCLUDE_DIR=${lua5_3_compat}/include .. && make && make install
    cd ../..
  '';
  installPhase = ''
    echo "ok"
  '';

  meta = with lib; {
    description = "Next generation of fcitx";
    homepage = "https://github.com/fcitx/fcitx5";
    license = licenses.lgpl21Plus;
    maintainers = with maintainers; [ poscat ];
    platforms = platforms.linux;
  };
}
